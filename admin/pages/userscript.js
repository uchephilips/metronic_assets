/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function pn(num) {
    return parseInt(num);
}



function checker($scope, $http) {


    $scope.loadTable = function () {
        $http.get($('#baseDir').val()+"/index.php/users/getTable?userz&tableLength=" + $scope.tableLength + "&tableStart=" + $scope.start + "&tableSearch=" + $('#tableSearch').val())
                .success(function (response) {
                    $("#loadR").hide();
                    $scope.count = response.count;
                    $scope.records = [{}];
                    $scope.records.splice(0, 1);
                    if (response.records.length != 0) {
                        response.records.forEach(function (record) {

                            $scope.lm = true;
                            $scope.record_break += 1;

                            $scope.records.push(record);
                            if ($scope.record_break >= $scope.count)
                                $scope.record_break = $scope.count;
                        });
                        if ($scope.record_break == $scope.count) {
                            $scope.lm = false;
                            $('#table_next').hide();
                        } else {
                            $scope.lm = true;
                            $('#table_next').show();
                        }
                    } else {
                        $scope.lm = false;
                        $('#table_next').hide();
                    }

                    $scope.totalRec = $scope.records.length;

                });
    };


    $scope.loadMoreRecord = function () {
        $scope.databaseLimit += 10;
        $("#loadR").show();
        if ($scope.lm) {
            $http.get("getTable?userz"
                    + "&tableLength=" + $scope.tableLength
                    + "&tableStart=" + $scope.record_break
                    + "&tableSearch=" + $('#tableSearch').val())
                    .success(function (response) {
                        $("#loadR").hide();
                        $scope.count = response.count;
                        if (response.records.length != 0) {
                            response.records.forEach(function (record) {

                                $scope.lm = true;
                                $scope.record_break += 1;
                                $scope.totalRec++;
                                $scope.records.push(record);
                            });
                            if ($scope.record_break == $scope.count) {
                                $scope.lm = false;
                                $('#table_next').hide();
                            } else {
                                $scope.lm = true;
                                $('#table_next').show();
                            }
                        } else {
                            $scope.lm = false;
                            $('#table_next').hide();
                        }

                        $scope.totalRec = $scope.records.length;
                    });
        }
    };


}






